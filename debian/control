Source: python-blazarclient
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-cliff,
 python3-coverage <!nocheck>,
 python3-fixtures <!nocheck>,
 python3-hacking,
 python3-keystoneauth1,
 python3-mock <!nocheck>,
 python3-openstackdocstheme <!nodoc>,
 python3-osc-lib,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.utils,
 python3-oslotest <!nocheck>,
 python3-prettytable,
 python3-reno <!nodoc>,
 python3-testtools <!nocheck>,
 python3-stestr <!nocheck>,
 subunit <!nocheck>,
Standards-Version: 4.2.0
Vcs-Browser: https://salsa.debian.org/openstack-team/clients/python-blazarclient
Vcs-Git: https://salsa.debian.org/openstack-team/clients/python-blazarclient.git
Homepage: https://github.com/openstack/python-blazarclient

Package: python3-blazarclient
Architecture: all
Depends:
 python3-cliff,
 python3-keystoneauth1,
 python3-osc-lib,
 python3-oslo.i18n,
 python3-oslo.log,
 python3-oslo.utils,
 python3-pbr,
 python3-prettytable,
 ${misc:Depends},
 ${python3:Depends},
Description: client for OpenStack Reservation Service
 Blazar is a resource reservation service for OpenStack. Blazar enables users
 to reserve a specific type/amount of resources for a specific time period and
 it leases these resources to users based on their reservations.
 .
 The following two resource types are currently supported:
  * Compute host: reserve and lease with a unit of a whole host
  * Instance: reserve and lease with a unit of a flavor
 .
 This package contains the client Python module and cli tool.
